package models

type Car struct {
	Brand    string `json:"brand" gorm:"not null"`
	Model    string `json:"model" gorm:"not null"`
	OwnerId  int    `json:"owner_id" gorm:"not null"`
	Color    string `json:"color" gorm:"not null"`
	FuelType string `json:"fuel_type" gorm:"not null"`
	New      int    `json:"new" gorm:"not null"`
}

type JsonCar struct {
	Brand    string `json:"brand"`
	Model    string `json:"model"`
	Color    string `json:"color"`
	FuelType string `json:"fuel_type"`
	New      bool   `json:"new"`
}
