package models

type Owner struct {
	Id        int    `json:"id" gorm:"not null"`
	FirstName string `json:"first_name"  binding:"required" gorm:"not null"`
	LastName  string `json:"last_name"  binding:"required" gorm:"not null"`
	BirthDate string `json:"birth_date"  binding:"required" gorm:"not null"` // format YYYY-MM-DD
	Gender    string `json:"gender"  binding:"required" gorm:"not null"`
}

type JsonOwner struct {
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Age       int       `json:"age"`
	Gender    string    `json:"gender"`
	Cars      []JsonCar `json:"cars"`
}
