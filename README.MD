
## Clone the project
```Shell
git clone git clone https://Artur_Chopikian@bitbucket.org/Artur_Chopikian/test-app.git
cd rest-api-app
```

## Install go(lang)

with [homebrew](http://mxcl.github.io/homebrew/):

```Shell
sudo brew install go
```

with [apt](http://packages.qa.debian.org/a/apt.html)-get:

```Shell
sudo apt-get install golang
```

[install Golang manually](https://golang.org/doc/install)
or
[compile it yourself](https://golang.org/doc/install/source)

## Install MAMP for run MySql server
Official site for download [MAMP](https://www.mamp.info/en/downloads/)

## Write your params in config/config.yml
```yml
port: "your port for site"

db:
  host: "your host"
  port: "your port"
  username: "your username"
  dbname: "your name of database"
```

## Create your .env file for save you password of db
```dotenv
DB_PASSWORD="your password"
```

## Run the project
```Shell
go run cmd/main.go
```

## Build and run project
```Shell
go build cmd/main.go
./main
```
