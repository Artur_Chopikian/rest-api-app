package repository

import (
	"github.com/ArturChopikian/test-app/models"
	"gorm.io/gorm"
)

type Authorization interface {
	CreateOwner(owner models.Owner) error
	CreateCar(car models.Car) error
}

type GettingInfo interface {
	GetOwner(firstName string, lastName string) (models.Owner, []models.JsonCar, error)
}

type Repository struct {
	Authorization
	GettingInfo
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		Authorization: NewAuthMySql(db),
		GettingInfo: NewGetInfoMySql(db),
	}
}
