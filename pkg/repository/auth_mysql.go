package repository

import (
	"github.com/ArturChopikian/test-app/models"
	"gorm.io/gorm"
)

const (
	ownersTable = "owners"
	carsTable   = "cars"
)

type AuthMySql struct {
	db *gorm.DB
}

func NewAuthMySql(db *gorm.DB) *AuthMySql {
	return &AuthMySql{db: db}
}

func (m *AuthMySql) CreateOwner(owner models.Owner) error {

	if err := m.AutoMigrate(); err != nil {
		return err
	}

	if err := m.db.Create(&owner).Error; err != nil {
		return err
	}

	return nil
}

func (m *AuthMySql) CreateCar(car models.Car) error {

	if err := m.AutoMigrate(); err != nil {
		return err
	}

	if err := m.db.Create(&car).Error; err != nil {
		return err
	}

	return nil
}

func (m *AuthMySql) AutoMigrate() error {
	if err := m.db.AutoMigrate(&models.Owner{}, &models.Car{}); err != nil {
		return err
	}
	return nil
}
