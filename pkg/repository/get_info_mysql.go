package repository

import (
	"fmt"
	"github.com/ArturChopikian/test-app/models"
	"gorm.io/gorm"
)

type GetInfoMySql struct {
	db *gorm.DB
}

func NewGetInfoMySql(db *gorm.DB) *GetInfoMySql {
	return &GetInfoMySql{db: db}
}

func (i *GetInfoMySql) GetOwner(firstName string, lastName string) (models.Owner, []models.JsonCar, error) {
	var owner models.Owner
	var cars []models.JsonCar

	if err := i.db.Table(ownersTable).First(&owner, fmt.Sprintf("first_name = '%s' AND last_name = '%s'", firstName, lastName)).Error; err != nil {
		return owner, cars, err
	}

	if err := i.db.Table(carsTable).Where("owner_id = ?", owner.Id).Find(&cars).Error; err != nil {
		return owner, cars, err
	}

	return owner, cars, nil
}
