package handler

import (
	"github.com/ArturChopikian/test-app/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h Handler) createCar(c *gin.Context) {
	var input models.Car

	if err := c.BindJSON(&input); err != nil {
		NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if err := h.services.Authorization.CreateCar(input); err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, map[string]interface{}{
		"detail": "car created",
	})
}
