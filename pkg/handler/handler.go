package handler

import (
	"github.com/ArturChopikian/test-app/pkg/service"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	api := router.Group("/api")
	{
		owner := api.Group("/owner")
		{
			owner.POST("", h.createOwner)
			owner.GET("", h.getOwner) // ?first_name=""&last_name=""
		}

		car := api.Group("/car")
		{
			car.POST("", h.createCar)
		}
	}
	return router
}
