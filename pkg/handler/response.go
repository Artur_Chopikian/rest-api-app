package handler

import (
	"github.com/gin-gonic/gin"
	"log"
)

type myError struct {
	Message string `json:"message"`
}

func NewErrorResponse(c *gin.Context, statusCode int, message string) {
	c.AbortWithStatusJSON(statusCode, myError{message})
	log.Panicf("%s", message)
}
