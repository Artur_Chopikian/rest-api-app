package handler

import (
	"fmt"
	"github.com/ArturChopikian/test-app/models"
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
	"time"
)

func (h Handler) createOwner(c *gin.Context) {
	var input models.Owner

	if err := c.BindJSON(&input); err != nil {
		NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if err := h.services.Authorization.CreateOwner(input); err != nil {
		NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, map[string]interface{}{
		"detail": "owner created",
	})
}

func (h Handler) getOwner(c *gin.Context) {

	params := c.Request.URL.Query()

	owner, cars, err := h.services.GettingInfo.GetOwner(params["first_name"][0], params["last_name"][0])
	if err != nil {
		c.JSON(http.StatusNotFound, map[string]interface{}{
			"detail": "not found",
		})
		return
	}

	age := CalculateAge(owner.BirthDate)

	c.JSON(http.StatusOK, map[string]interface{}{
		"result": models.JsonOwner{
			FirstName: owner.FirstName,
			LastName:  owner.LastName,
			Age:       age,
			Gender:    owner.Gender,
			Cars:      cars,
		},
		"total_count": len(cars),
	})
}

func CalculateAge(birthday string) int {

	layout := "2006-01-02"

	birthdayUTC, _ := time.Parse(layout, fmt.Sprintf("%s", birthday))

	age := int(math.Floor(time.Now().Sub(birthdayUTC).Hours() / 24 / 365))

	return age
}
