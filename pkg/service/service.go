package service

import (
	"github.com/ArturChopikian/test-app/models"
	"github.com/ArturChopikian/test-app/pkg/repository"
)

type Authorization interface {
	CreateOwner(owner models.Owner) error
	CreateCar(car models.Car) error
}

type GettingInfo interface {
	GetOwner(firstName string, lastName string) (models.Owner, []models.JsonCar, error)
}

type Service struct {
	Authorization
	GettingInfo
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repos.Authorization),
		GettingInfo:   NewGetInfoService(repos.GettingInfo),
	}
}
