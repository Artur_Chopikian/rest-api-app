package service

import (
	"github.com/ArturChopikian/test-app/models"
	"github.com/ArturChopikian/test-app/pkg/repository"
)

type GetInfoService struct {
	repo repository.GettingInfo
}

func NewGetInfoService(repo repository.GettingInfo) *GetInfoService {
	return &GetInfoService{repo: repo}
}

func (s *GetInfoService) GetOwner(firstName string, lastName string) (models.Owner, []models.JsonCar, error) {
	return s.repo.GetOwner(firstName, lastName)
}
