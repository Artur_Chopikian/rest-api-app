package service

import (
	"github.com/ArturChopikian/test-app/models"
	"github.com/ArturChopikian/test-app/pkg/repository"
)

type AuthService struct {
	repo repository.Authorization
}

func NewAuthService(repo repository.Authorization) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) CreateOwner(owner models.Owner) error {
	return s.repo.CreateOwner(owner)
}

func (s *AuthService) CreateCar(car models.Car) error {
	return s.repo.CreateCar(car)
}
